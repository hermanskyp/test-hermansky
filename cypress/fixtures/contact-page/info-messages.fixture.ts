export enum CONTACT_PAGE_MESSAGES_FX {
  ERROR_ALERT_ALL_INFO_SHALL_BE_ENTERED = "All items shall be entered.",
  ERROR_ALERT_PERSONAL_DATA = "You must agree with the processing of personal data.",
  ERROR_SPAM = "Sorry, it is a SPAM!",
  SUCCESS_SENT = "The message was successfuly sent, thank you!"
}
