import { UiRoutes } from "cypress/support/enums/ui-routes";
import { TERMS_PAGE_LOCATORS } from "cypress/support/uat/terms-page/terms-page.locators";

describe("Terms Page - User Stories", function () {
  beforeEach(function () {
    cy.log("Given terms page open");
    cy.visit(UiRoutes.TERMS_PAGE);
    cy.acceptCookieViaButton();
  });

  it(`UC12 - ${UiRoutes.THIRD_PARTIES_PAGE} link should be valid`, function () {
    cy.log(`Then the link ${UiRoutes.THIRD_PARTIES_PAGE} should be valid`);
    cy.get(TERMS_PAGE_LOCATORS.divUseTermsContent)
      .find("a")
      .contains("here")
      .should("have.attr", "href")
      .and("include", UiRoutes.THIRD_PARTIES_PAGE)
      .then(link => {
        console.log(link);
        cy.request(`${link}`) //
          .its("status")
          .should("eq", 200);
      });
  });

  it(`UC13 - ${UiRoutes.STORAGE_PAGE} link should be valid`, function () {
    cy.log(`Then the link ${UiRoutes.STORAGE_PAGE} should be valid`);
    cy.get(TERMS_PAGE_LOCATORS.divUseTermsContent)
      .find("a")
      .contains("Cookie Policy")
      .should("have.attr", "href")
      .and("include", UiRoutes.STORAGE_PAGE)
      .then(link => {
        console.log(link);
        cy.request(`${link}`) //
          .its("status")
          .should("eq", 200);
      });
  });
});
