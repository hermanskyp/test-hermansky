import { TermsPageCommands } from "cypress/support";
import { UiRoutes } from "cypress/support/enums/ui-routes";
import getDeviceConfig from "cypress/support/utils/get-device-config";

const { name } = getDeviceConfig();

describe("Terms Page - Visual", function () {
  beforeEach(function () {
    cy.visit(UiRoutes.TERMS_PAGE);
  });

  it("UC11 - User should see terms of use", function () {
    cy.acceptCookieViaButton() //
      .setViewportFromEnv();

    // Take and match snapshot
    // I would consider here to use some third party app (aplitools, percy) if it would be in the budget
    TermsPageCommands.getTermsContentDiv() //
      .matchImageSnapshot(`use-terms-page-content-${name}`, {
        capture: "fullPage"
      });
  });
});
