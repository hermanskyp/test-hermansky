import { UiRoutes } from "cypress/support/enums/ui-routes";
import { faker } from "@faker-js/faker";
import { CONTACT_PAGE_MESSAGES_FX } from "cypress/fixtures/contact-page/info-messages.fixture";
import { ContactPageCommands } from "cypress/support";

describe("Contact Page - User Stories", function () {
  beforeEach(function () {
    cy.visit(UiRoutes.CONTACT_PAGE);
  });

  it("UC1 - User should be able to send a message", function () {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.exampleEmail(firstName, lastName);
    const testText = faker.lorem.words(3);

    cy.log("When the user fills in all required information");
    ContactPageCommands.fillInMessage(0);
    ContactPageCommands.fillInText(testText);
    ContactPageCommands.fillInName(`${firstName} ${lastName}`);
    ContactPageCommands.fillInEmail(email);

    ContactPageCommands.checkConsent() //
      .should("be.checked");

    ContactPageCommands.submitMessage({ log: "And sends the message" });
    // There would need to be some mechanism for test environment where it would not return SPAM error message
    // or some possibility to stub the message

    ContactPageCommands.isContactFormMessageVisible(true, {
      log: "Then the message is successfully sent"
    });

    // .should("contain", CONTACT_PAGE_MESSAGES_FX.SUCCESS_SENT);
  });

  it("UC2 - User should not be able to send an empty form", function () {
    ContactPageCommands.submitMessage({
      log: "When the user tries to send an empty form"
    });

    cy.log("Then the alert should be visible");
    cy.on("window:alert", textToVerify => {
      expect(textToVerify).eq(
        CONTACT_PAGE_MESSAGES_FX.ERROR_ALERT_ALL_INFO_SHALL_BE_ENTERED
      );
    });
  });
});
