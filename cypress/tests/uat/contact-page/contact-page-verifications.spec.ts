import faker from "@faker-js/faker";
import { CONTACT_PAGE_MESSAGES_FX } from "cypress/fixtures/contact-page/info-messages.fixture";
import { ContactPageCommands } from "cypress/support";
import { UiRoutes } from "cypress/support/enums/ui-routes";
import { CONTACT_PAGE_LOCATORS } from "cypress/support/uat/contact-page/contact-page.locators";

const invalidEmailList: { id: string; email: string }[] = [
  { id: "UC3", email: "a" },
  { id: "UC4", email: "a@" },
  { id: "UC5", email: "a@a" }
];

const sendAndVerifyAlert = () => {
  cy.log("And tries to send the message");
  ContactPageCommands.submitMessage();

  cy.log("Then the alert should be visible");
  cy.on("window:alert", textToVerify => {
    expect(textToVerify).eq(
      CONTACT_PAGE_MESSAGES_FX.ERROR_ALERT_ALL_INFO_SHALL_BE_ENTERED
    );
  });
};

describe("Contact Page - Verifications", function () {
  beforeEach(function () {
    cy.log("Given contact page open");
    cy.visit(UiRoutes.CONTACT_PAGE);
  });

  invalidEmailList.forEach(emailObj => {
    const { id, email } = emailObj;
    it(`${id} - User should not be able to enter invalid email like "${email}"`, function () {
      cy.log(`When the user enters ${email} to email`);
      ContactPageCommands.fillInEmail(email);
      ContactPageCommands.submitMessage();

      cy.log("Then the error is visible");
      // there should be check of the error but I would need to see the implementation.
      // I can't find it in the DOM" - it looks like some default err pop-up
      ContactPageCommands.isContactFormMessageVisible(false, {
        log: "And the info message is not visible"
      });
    });
  });

  it("UC5 - User should not be able to send the message with only filled subject", function () {
    cy.log("When the user fills in only subject");
    ContactPageCommands.fillInMessage(0);

    sendAndVerifyAlert();
  });

  it("UC6 - User should not be able to send the message with only filled subject and text", function () {
    cy.log("When the user fills in only subject and text");
    ContactPageCommands.fillInMessage(0);
    ContactPageCommands.fillInText(faker.lorem.word(2));

    sendAndVerifyAlert();
  });

  it("UC7 - User should not be able to send the message with only filled subject, text, and name", function () {
    cy.log("When the user fills in only subject, text, and name");
    ContactPageCommands.fillInMessage(0);
    ContactPageCommands.fillInText(faker.lorem.word(2));
    ContactPageCommands.fillInName(faker.name.findName());

    sendAndVerifyAlert();
  });

  it("UC8 - User should not be able to send the message with only filled subject, text, name, and email", function () {
    cy.log("When the user fills in only subject, text, name, and email");
    ContactPageCommands.fillInMessage(0);
    ContactPageCommands.fillInText(faker.lorem.word(2));
    ContactPageCommands.fillInName(faker.name.findName());
    ContactPageCommands.fillInEmail(faker.internet.email());

    cy.log("Then the alert should be visible");
    cy.on("window:alert", textToVerify => {
      expect(textToVerify).eq(
        CONTACT_PAGE_MESSAGES_FX.ERROR_ALERT_PERSONAL_DATA
      );
    });
  });

  it("UC9 - Link in contact header is valid", function () {
    cy.log("Then the link should be valid");
    cy.get(CONTACT_PAGE_LOCATORS.contactSection.contactText)
      .find("a")
      .should("have.attr", "href")
      .and("include", UiRoutes.LINK_ADVERTISE_PAGE)
      .then(link => {
        console.log(link);
        cy.request(`${link}`) //
          .its("status")
          .should("eq", 200);
      });
  });

  it("UC10 - Link for personal data is valid", function () {
    cy.log("Then the link should be valid");
    cy.get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.checkboxConsent)
      .parent()
      .find("a")
      .should("have.attr", "href")
      .and("include", UiRoutes.PERSONAL_DATA_PAGE)
      .then(link => {
        console.log(link);
        cy.request(`${link}`) //
          .its("status")
          .should("eq", 200);
      });
  });
});
