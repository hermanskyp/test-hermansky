enum Device {
	MOBILE = "mobile",
	DESKTOP = "desktop",
}

export default Device;
