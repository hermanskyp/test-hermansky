export enum UiRoutes {
  CONTACT_PAGE = "contact",
  TERMS_PAGE = "terms",

  LINK_ADVERTISE_PAGE = "advertise",
  PERSONAL_DATA_PAGE = "personal-data",

  THIRD_PARTIES_PAGE = "third-parties",
  STORAGE_PAGE = "storage"
}
