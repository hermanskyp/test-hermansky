import Device from "../enums/devices";
import { DESKTOP, MOBILE } from "../devices";

const getDeviceConfig = () => {
	const device = Cypress.env("DEVICE");

	switch (device) {
		case Device.MOBILE:
			return MOBILE;

		case Device.DESKTOP:
			return DESKTOP;

		default:
			throw new Error(`Unknown device "${device}".`);
	}
};

export default getDeviceConfig;
