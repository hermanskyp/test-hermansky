export const TERMS_PAGE_LOCATORS = {
  divUseTerms: ".useTerms",
  divUseTermsHeader: ".useTerms__header",
  divUseTermsContent: ".useTerms__content"
};
