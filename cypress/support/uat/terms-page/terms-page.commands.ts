import { TERMS_PAGE_LOCATORS } from "./terms-page.locators";

export const getTermsContentDiv = () => {
  return cy.get(TERMS_PAGE_LOCATORS.divUseTerms);
};
