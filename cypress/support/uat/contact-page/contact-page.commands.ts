import { MessageListFx } from "cypress/fixtures/contact-page/message-subjects.fixture";
import { CONTACT_PAGE_LOCATORS } from "./contact-page.locators";

// possible to move that to more general place - refactoring
interface OptionalLog {
  log?: string;
}

export const fillInMessage = (messageIndex = 0, opts?: OptionalLog): void => {
  cy.log(opts?.log ?? `Fill in text ${MessageListFx[messageIndex]}`);
  cy.get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.selectSubject) //
    .click();
  cy.contains(MessageListFx[messageIndex]) //
    .click();
};

export const fillInText = (
  text: string,
  opts?: OptionalLog
): Cypress.Chainable<JQuery<HTMLElement>> => {
  cy.log(opts?.log ?? `Fill in text ${text}`);
  return cy
    .get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.inputText) //
    .type(text);
};

export const fillInName = (
  name: string,
  opts?: OptionalLog
): Cypress.Chainable<JQuery<HTMLElement>> => {
  cy.log(opts?.log ?? `Fill in name ${name}`);
  return cy
    .get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.inputName) //
    .type(`${name}`);
};
export const fillInEmail = (
  email: string,
  opts?: OptionalLog
): Cypress.Chainable<JQuery<HTMLElement>> => {
  cy.log(opts?.log ?? `Fill in email ${email}`);
  return cy
    .get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.inputEmail) //
    .type(`${email}`);
};

export const checkConsent = (): Cypress.Chainable<JQuery<HTMLElement>> => {
  return cy
    .get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.checkboxConsent) //
    .check();
};

export const submitMessage = (
  opts?: OptionalLog
): Cypress.Chainable<JQuery<HTMLElement>> => {
  cy.log(opts?.log ?? `Submits the form`);
  return cy
    .get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.buttonSubmit) //
    .click();
};

// Checks
export const isContactFormMessageVisible = (
  isVisible = true,
  opts?: OptionalLog
) => {
  cy.log(
    opts?.log ??
      `Contact message form should ${isVisible ? "be visible" : "not exist"}`
  );
  cy.get(CONTACT_PAGE_LOCATORS.mainSection.contactForm.message) //
    .should(isVisible ? "be.visible" : "not.exist");
};
