export const CONTACT_PAGE_LOCATORS = {
  header: ".header",
  menuTop: ".menuTop",
  main: "#main",
  contactSection: {
    contactHeader: ".contactHeader__heading",
    contactText: ".contactHeader__text"
  },
  mainSection: {
    contactFormLocator: "#Contact",
    contactForm: {
      message: "#message-0",
      selectSubject: ".contactWrapper__information",
      inputText: "#Text",
      inputName: "#Name",
      inputEmail: "#Email",
      checkboxConsent: "#Consent",
      buttonSubmit: "#Submit"
    }
  }
};
