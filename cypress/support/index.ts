import "cypress-mochawesome-reporter/register";
import "./commands";
import "cypress-real-events/support";
import getDeviceConfig from "../support/utils/get-device-config";

import "./test-hooks";

import * as ContactPageCommands from "./uat/contact-page/contact-page.commands";
import * as TermsPageCommands from "./uat/terms-page/terms-page.commands";

const resizeObserverLoopErrRe = /^[^(ResizeObserver loop limit exceeded)]/;
Cypress.on("uncaught:exception", err => {
  /* returning false here prevents Cypress from failing the test */
  if (resizeObserverLoopErrRe.test(err.message)) {
    return false;
  }
});

const { dimensions } = getDeviceConfig();

Cypress.config("viewportWidth", dimensions[0]);
Cypress.config("viewportHeight", dimensions[1]);

export { ContactPageCommands, TermsPageCommands };
