import { addMatchImageSnapshotCommand } from "cypress-image-snapshot/command";
import "@testing-library/cypress/add-commands";

import { configure } from "@testing-library/cypress";
import getDeviceConfig from "./utils/get-device-config";
// If there would be an option to add custom data attributes to FE
configure({ testIdAttribute: "data-cy" });

addMatchImageSnapshotCommand({
  failureThreshold: 0.03,
  failureThresholdType: "percent",
  customDiffConfig: { threshold: 0.2 },
  capture: "fullPage"
});

Cypress.Commands.add("acceptCookieViaButton", () => {
  cy.log("Accepting cookie");
  cy.get("#onetrust-accept-btn-handler") //
    .click();
  cy.log("cookie banner should not be visible");
  cy.get("#onetrust-banner-sdk") //
    .should("not.be.visible");
});

Cypress.Commands.add("setViewportFromEnv", () => {
  const { dimensions } = getDeviceConfig();
  cy.viewport(dimensions[0], dimensions[1]);
});

/**
 * This will allow seeing the reproduction steps if the test fails for whatever reason
 */
Cypress.Commands.overwrite("log", (originalFn, message) => {
  Cypress.log({
    displayName: `--- ${
      (window as any).logCalls
    }. ${message} ---`.toUpperCase(),
    name: `--- ${(window as any).logCalls}. ${message} ---`.toUpperCase(),
    message: ""
  });
});
