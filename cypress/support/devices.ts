import Device from "./enums/devices";
import type { DeviceType } from "../../types/devices";

export const MOBILE: DeviceType = { name: Device.MOBILE, dimensions: [360, 640], isResponsive: true };
export const DESKTOP: DeviceType = { name: Device.DESKTOP, dimensions: [1280, 1024], isResponsive: false };
export default [MOBILE, DESKTOP];
