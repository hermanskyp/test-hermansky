import Device from "cypress/support/enums/devices";

export type DeviceType = {
  name: Device;
  dimensions: [number, number];
  isResponsive: boolean;
};
