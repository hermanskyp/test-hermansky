declare namespace Cypress {
  interface Chainable {
    acceptCookieViaButton(): Cypress.Chainable;
    setViewportFromEnv(): Cypress.Chainable;
  }
}
