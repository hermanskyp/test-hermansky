# test-automation

## Prerequisites

- Node and NPM installed (<https://nodejs.org/en/>)
- Recommended Node version 16.14.0

## Getting started

1. Install project dependencies:

```console
cd test-automation && npm install
```

2. Run your test with Cypress GUI test runner:

```console
npm run cy:open
```

## Info & tips

- Your test file `cypress/integration/faq-page.spec.ts` :)
- Snapshots from your test are in folder: `cypress/snapshots/`
- For click and hover events use cypress-real-events library
- For screenshots use cypress-image-snapshot library
- If you have any problems with the project, do not hesitate to contact us :)

## Available Cypress CLI scripts

### Headed mode

To run your test in headed mode use command:

```console
npm run cy:open
```

### Headless mode

To run your test in headless mode use command:

```console
npm run cy:run
```

## Tests

automation tests are located under ```cypress/tests/uat/**```

### contact page

User scenarios:

- UC1 - User should be able to send a message
- UC2 - User should not be able to send an empty form

Verifications:

- UC3 - User should not be able to enter invalid email like "a"
- UC4 - User should not be able to enter invalid email like "a@"
- UC4 - User should not be able to enter invalid email like "a@a"
- UC4 - User should be able to enter invalid email like "test@test.com"
- UC5 - User should not be able to send the message with only filled subject
- UC6 - User should not be able to send the message with only filled subject and text
- UC7 - User should not be able to send the message with only filled subject, text, and name
- UC8 - User should not be able to send the message with only filled subject, text, name, and email
- UC9 - Link in contact header is valid
- UC10 - Link for personal data is valid

### terms page

Visuals:

- UC11 - User should see terms of use

Verifications:  

- UC12 - third-party link should be valid`
- UC13 - storage link should be valid`
